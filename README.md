# pcap to csv

## Setup development environment

* Docker _(v19.03.3 tested)_

If you want to compile the Go binary without Docker, you must donwload the library`libpcap-dev`

__On linux__

```$ sudo apt-get install -y libpcap-dev```

## Description

this tool allows you to convert smartball datagram captured in a pcap file to csv

by default it convert all files from a directory, but you can convert just one using the 'FILE_UNIQUE_PATH' environment variable

## Author

Guillaume Picard
guillaume.picard@ecole.ensicaen.fr

## Environment variables

| variable | description | default value |
| -------- | ----------- | ------------- |
| FILE_TYPE | type of files to convert | pcap |
| ASSETS_PATH | path to a directory containing files to convert | /usr/local/smartball/assets |
| FILE_UNIQUE_PATH | path to a unique file to convert |  |

## Docker

### Build

```
docker build -t pcap-to-csv .
```

### Run

```
docker run -it --name pcap_to_csv pcap-to-csv
```
