
all: dep build run

dep:
	go mod download
build:
	GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/pcap-to-csv/cmd/pcap-to-csv
run:
	go run gitlab.com/smartballs/pcap-to-csv/cmd/pcap-to-csv
test:
	go test gitlab.com/smartballs/pcap-to-csv/... -v -coverprofile .coverage.txt
	go tool cover -func .coverage.txt
coverage: test
	go tool cover -html=.coverage.txt