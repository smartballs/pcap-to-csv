package pcaptocsv_test

import (
	pcaptocsv "gitlab.com/smartballs/pcap-to-csv/internal/pcap-to-csv"
	"testing"
)

func TestStripTrailingSlash(t *testing.T) {
	type dataItems struct {
		input    string
		expected string
	}

	data := []dataItems{
		{"/", ""},
		{"/data", "/data"},
		{"", ""},
		{"//", "/"},
		{"/data/", "/data"},
	}

	for _, item := range data {
		result := pcaptocsv.StripTrailingSlash(item.input)

		if result != item.expected {
			t.Errorf("expected: '%s' got:'%s' with: '%s'", item.expected, result, item.input)
		}
	}
}

func TestFloat64ToStringRounded(t *testing.T) {
	type dataItems struct {
		f64      float64
		round    int
		expected string
	}

	data := []dataItems{
		{3.1415926, 2, "3.14"},
		{3.1415926, 4, "3.1416"},
		{3.1415426, 4, "3.1415"},
		{3.1415926, 1, "3.14"},
	}

	for _, item := range data {
		result := pcaptocsv.Float64ToStringRounded(item.f64, item.round)

		if result != item.expected {
			t.Errorf("expected: '%s' got:'%s' for: '%f - %d'", item.expected, result, item.f64, item.round)
		}
	}
}
