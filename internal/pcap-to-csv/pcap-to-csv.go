package pcaptocsv

import (
	"encoding/csv"
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	log "github.com/sirupsen/logrus"
	sbudp "gitlab.com/smartballs/driver/pkg/smartball/sb-udp"
	"gitlab.com/smartballs/pcap-to-csv/config"
	"os"
	"path/filepath"
	"strconv"
)

type packetData struct {
	timestamp int64
	data      []byte
}

func Start() {
	if len(config.Common.FileUniquePath) > 0 {
		log.Infof("Handle a unique file : %s", config.Common.FileUniquePath)

		packets := getPackets(config.Common.FileUniquePath)
		data := getData(packets)
		parse := parseData(data)
		saveData(config.Common.FileUniquePath, parse)
	} else {
		pattern := StripTrailingSlash(config.Common.AssetsPath) + "/*." + config.Common.FileType

		files, err := filepath.Glob(pattern)
		if err != nil {
			log.Fatalf("Cannot load pcap files with pattern : %s", pattern)
		}

		for _, file := range files {
			packets := getPackets(file)
			data := getData(packets)
			parse := parseData(data)
			saveData(file, parse)
		}
	}
}

func saveData(filePath string, parse [][]string) {
	substrFilePath := []rune(filePath)
	filePath = string(substrFilePath[0 : len(filePath)-len(config.Common.FileType)])
	filePath += "csv"

	file, err := os.Create(filePath)
	if err != nil {
		log.Fatalf("Unable to create file: %s", filePath)
	}
	defer func() {
		err := file.Close()
		if err != nil {
			log.Error("Unable to close properly the file")
		}
		log.Infof("Saved path: %s", filePath)
	}()

	writer := csv.NewWriter(file)
	writer.Flush()

	for _, value := range parse {
		_ = writer.Write(value)
	}
}

func parseData(packetData []packetData) [][]string {
	parsedData := [][]string{{"frame", "timestamp (μs)",
		"accX", "accY", "accZ", // "accN",
		"gyrX", "gyrY", "gyrZ", // "gyrN",
		"magX", "magY", "magZ", // "magN",
		"quaW", "quaX", "quaY", "quaZ",
		"wldX", "wldY", "wldZ",
		"tmp"}}

	for index, data := range packetData {
		packet := sbudp.Unmarshal(data.data)

		if packet.Command == sbudp.CmdDImu {
			pData, ok := packet.Data.(sbudp.ImuData)
			if !ok {
				log.Warn("Cannot cast IMU data")
				continue
			}

			d := []string{strconv.Itoa(index + 1), fmt.Sprintf("%d", data.timestamp),
				Float64ToStringRounded(pData.Acc.X, 2), Float64ToStringRounded(pData.Acc.Y, 2), Float64ToStringRounded(pData.Acc.Z, 2), // Float64ToStringRounded(pData.AccN, 2),
				Float64ToStringRounded(pData.Gyr.X, 2), Float64ToStringRounded(pData.Gyr.Y, 2), Float64ToStringRounded(pData.Gyr.Z, 2), // Float64ToStringRounded(pData.GyrN, 2),
				Float64ToStringRounded(pData.Mag.X, 2), Float64ToStringRounded(pData.Mag.Y, 2), Float64ToStringRounded(pData.Mag.Z, 2), // Float64ToStringRounded(pData.MagN, 2),
				Float64ToStringRounded(pData.Qua.X, 4), Float64ToStringRounded(pData.Qua.Y, 4), Float64ToStringRounded(pData.Qua.Z, 4), Float64ToStringRounded(pData.Qua.W, 4),
				Float64ToStringRounded(pData.Wld.X, 2), Float64ToStringRounded(pData.Wld.Y, 2), Float64ToStringRounded(pData.Wld.Z, 2),
				Float64ToStringRounded(pData.Tmp, 2)}

			parsedData = append(parsedData, d)
		}
	}

	return parsedData
}

func getData(handle *pcap.Handle) []packetData {
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	var packetsData []packetData

	for packet := range packetSource.Packets() {
		timestamp := packet.Metadata().Timestamp.UnixNano()
		timestamp /= 1000

		var data []byte

		for _, layer := range packet.Layers() {
			if layer.LayerType().String() == "Payload" {
				data = layer.LayerContents()
				break
			}
		}

		packetsData = append(packetsData, packetData{timestamp: timestamp, data: data})
	}

	return packetsData
}

func getPackets(path string) *pcap.Handle {
	log.Infof("Reading path: %s", path)

	handle, err := pcap.OpenOffline(path)
	if err != nil {
		log.Fatalf("Unable to open pcap path: %s", path)
	}

	return handle
}

// Strips the trailing slash on the provided path if there's one.
func StripTrailingSlash(path string) string {

	lastCharacterIndex := len(path) - 1
	if lastCharacterIndex < 0 {
		return path
	}

	if path[lastCharacterIndex] == '/' {
		runes := []rune(path)
		stripPath := string(runes[0:lastCharacterIndex])

		return stripPath
	} else {
		return path
	}
}

// Converts float64 to string and fix numbers after comma
func Float64ToStringRounded(f64 float64, fixed int) string {
	switch fixed {
	case 4:
		return fmt.Sprintf("%.4f", f64)
	default:
		return fmt.Sprintf("%.2f", f64)
	}
}
