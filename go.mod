module gitlab.com/smartballs/pcap-to-csv

go 1.13

require (
	github.com/google/gopacket v1.1.17
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/smartballs/driver v0.0.2
)
