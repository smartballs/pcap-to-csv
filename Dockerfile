FROM golang:1.13.1 as build

WORKDIR /app/bin

COPY . .

RUN apt-get update -y && apt-get upgrade -y && apt-get install -y libpcap-dev

RUN GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/pcap-to-csv/cmd/pcap-to-csv

FROM ubuntu:19.04

COPY --from=build /app/bin/pcap-to-csv /app/bin/pcap-to-csv
COPY assets /usr/local/smartball/assets

RUN apt-get -y update && apt-get -y upgrade && apt-get install -y libpcap-dev

ENTRYPOINT ["/app/bin/pcap-to-csv"]
