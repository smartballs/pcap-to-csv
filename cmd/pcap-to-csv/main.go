package main

import (
	pcaptocsv "gitlab.com/smartballs/pcap-to-csv/internal/pcap-to-csv"
)

func main() {
	pcaptocsv.Start()
}
