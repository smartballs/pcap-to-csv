package config

import (
	"gitlab.com/smartballs/pcap-to-csv/pkg/cfg"
)

var Common = struct {
	FileType       string
	FileUniquePath string
	AssetsPath     string
}{
	FileType:       cfg.Str("FILE_TYPE").ByDefault("pcap"),
	FileUniquePath: cfg.Str("FILE_UNIQUE_PATH").ByDefault(""), // If we set withoutDefault, program will crash
	AssetsPath:     cfg.Str("ASSETS_PATH").ByDefault("/usr/local/smartball/assets"),
}
